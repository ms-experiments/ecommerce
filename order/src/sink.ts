import {
  connect,
  Stan,
  Message,
  SubscriptionOptions,
} from "node-nats-streaming";

export const broker: Stan = connect("test-cluster", "order-svc", {
  url: "nats://broker:4222",
});

export function setupBroker(db: any) {
  broker.on("connect", () => {
    const replayAll: SubscriptionOptions = broker
      .subscriptionOptions()
      .setDeliverAllAvailable();

    broker.subscribe("ORDER_CREATED", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const { message }: any = JSON.parse(msg.getData() as string);

        await db.collection("orders").insertOne(message);
      }
    );

    broker.subscribe("CUSTOMER_NOT_FOUND", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const {
          message: { uuid },
        }: any = JSON.parse(msg.getData() as string);
        await db
          .collection("orders")
          .findOneAndUpdate({ uuid }, { $set: { status: "CANCELLED" } });
      }
    );

    broker.subscribe("CREDIT_LIMIT_EXCEEDED", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const {
          message: { uuid },
        }: any = JSON.parse(msg.getData() as string);
        await db
          .collection("orders")
          .findOneAndUpdate({ uuid }, { $set: { status: "DENIED" } });
      }
    );

    broker.subscribe("CREDIT_RESERVED", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const { message }: any = JSON.parse(msg.getData() as string);
        const { uuid }: any = message;

        await db
          .collection("orders")
          .findOneAndUpdate({ uuid }, { $set: { status: "APPROVED" } });

        const event: any = { type: "ORDER_PLACED", message };
        broker.publish(event.type, JSON.stringify(event));
      }
    );

    broker.subscribe("PACKAGE_READY", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const {
          message: {
            order: { uuid },
          },
        }: any = JSON.parse(msg.getData() as string);

        await db
          .collection("orders")
          .findOneAndUpdate(
            { uuid },
            { $set: { status: "READY_TO_DELIVERY" } }
          );
      }
    );

    broker.subscribe("PACKAGE_OUT_FOR_DELIVERY", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const {
          message: {
            motoboy,
            package: { order },
          },
        }: any = JSON.parse(msg.getData() as string);

        console.log(order);

        await db
          .collection("orders")
          .findOneAndUpdate(
            { uuid: order.uuid },
            { $set: { status: "ON_THE_WAY", motoboy: motoboy.name } }
          );
      }
    );
  });
}
