import { MongoClient } from "mongodb";

const { DB_HOST }: any = process.env;

export async function setupDB() {
  const client = await MongoClient.connect(`mongodb://${DB_HOST}:27017`);
  const db = client.db("customer");

  return db;
}
