import * as Koa from "koa";
import * as bodyParser from "koa-bodyparser";
import * as json from "koa-json";
import * as logger from "koa-logger";
import * as cors from "@koa/cors";
import * as Router from "koa-router";
import { v4 as uuid } from "uuid";

export const app: Koa = new Koa();

app.use(json());
app.use(logger());
app.use(bodyParser());
app.use(cors());

const router: Router = new Router();

function validateOrder({ customer_id, items }: any): any {
  if (!customer_id) {
    return {
      isValid: false,
      reason: "invalid customer_id",
    };
  }

  if (!items || items.length <= 0) {
    return {
      isValid: false,
      reason: "invalid item list",
    };
  }

  return { isValid: true };
}

router.get(
  "/health",
  async (ctx: any): Promise<void> => {
    const dbStats = await ctx.db.stats();

    ctx.body = { db: dbStats };
  }
);

router.post(
  "/orders",
  async (ctx: any): Promise<void> => {
    const { isValid, reason }: any = validateOrder(ctx.request.body);

    if (!isValid) {
      ctx.body = { error: reason };
    } else {
      const order = {
        ...ctx.request.body,
        uuid: uuid(),
        status: "PENDING",
      };

      const event = {
        type: "ORDER_CREATED",
        message: order,
      };

      ctx.broker.publish(event.type, JSON.stringify(event));
      ctx.body = { order };
    }
  }
);

app.use(router.routes());
app.use(router.allowedMethods());
