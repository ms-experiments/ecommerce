import { app } from "./server";
import { setupDB } from "./db";
import { broker, setupBroker } from "./sink";

const { PORT }: any = process.env;
const port: number = PORT ? Number.parseInt(PORT) : 9090;

async function boostrap() {
  const db: any = await setupDB();

  setupBroker(db);

  app.context.broker = broker;
  app.context.db = db;

  app.listen(port);
}

boostrap();
