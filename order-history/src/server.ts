import * as Koa from "koa";
import * as bodyParser from "koa-bodyparser";
import * as json from "koa-json";
import * as logger from "koa-logger";
import * as cors from "@koa/cors";
import * as Router from "koa-router";

export const app: Koa = new Koa();

app.use(json());
app.use(logger());
app.use(bodyParser());
app.use(cors());

const router: Router = new Router();

router.get(
  "/health",
  async (ctx: any): Promise<void> => {
    const dbStats = await ctx.db.stats();

    ctx.body = { db: dbStats };
  }
);

router.get(
  "/orders",
  async (ctx: any): Promise<void> => {
    const { db }: any = ctx;
    const orders: any[] = await db.collection("orders").find({}).toArray();
    ctx.body = { orders };
  }
);

router.get(
  "/orders/:id",
  async (ctx: any): Promise<void> => {
    const { id }: any = ctx.params;

    const order: any = await ctx.db.collection("orders").findOne({ uuid: id });

    if (!order) {
      ctx.body = { error: "order not found - " + id };
    } else {
      ctx.body = { order };
    }
  }
);

app.use(router.routes());
app.use(router.allowedMethods());
