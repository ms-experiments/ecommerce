import {
  connect,
  Stan,
  Message,
  SubscriptionOptions,
} from "node-nats-streaming";

export const broker: Stan = connect("test-cluster", "order-history-svc", {
  url: "nats://broker:4222",
});

export function setupBroker(db: any) {
  broker.on("connect", () => {
    const replayAll: SubscriptionOptions = broker
      .subscriptionOptions()
      .setDeliverAllAvailable();

    broker.subscribe("ORDER_CREATED", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const { message }: any = JSON.parse(msg.getData() as string);

        const order: any = {
          uuid: message.uuid,
          items: message.items,
          customer_id: message.customer_id,
          history: [{ status: message.status, timestamp: new Date() }],
        };

        await db.collection("orders").insertOne(order);
      }
    );

    broker.subscribe("CUSTOMER_NOT_FOUND", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const {
          message: { uuid },
        }: any = JSON.parse(msg.getData() as string);
        await db.collection("orders").findOneAndUpdate(
          { uuid },
          {
            $push: {
              history: {
                status: "CANCELLED",
                timestamp: new Date(),
              },
            },
          }
        );
      }
    );

    broker.subscribe("CREDIT_LIMIT_EXCEEDED", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const {
          message: { uuid },
        }: any = JSON.parse(msg.getData() as string);
        await db.collection("orders").findOneAndUpdate(
          { uuid },
          {
            $push: {
              history: {
                status: "DENIED",
                timestamp: new Date(),
              },
            },
          }
        );
      }
    );

    broker.subscribe("CREDIT_RESERVED", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const { message }: any = JSON.parse(msg.getData() as string);
        const { uuid }: any = message;

        await db.collection("orders").findOneAndUpdate(
          { uuid },
          {
            $push: {
              history: {
                status: "APPROVED",
                timestamp: new Date(),
              },
            },
          }
        );
      }
    );

    broker.subscribe("PACKAGE_READY", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const {
          message: {
            order: { uuid },
          },
        }: any = JSON.parse(msg.getData() as string);

        await db.collection("orders").findOneAndUpdate(
          { uuid },
          {
            $push: {
              history: {
                status: "READY_TO_DELIVERY",
                timestamp: new Date(),
              },
            },
          }
        );
      }
    );

    broker.subscribe("PACKAGE_OUT_FOR_DELIVERY", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const {
          message: {
            motoboy,
            package: { order },
          },
        }: any = JSON.parse(msg.getData() as string);

        console.log(order);

        await db.collection("orders").findOneAndUpdate(
          { uuid: order.uuid },
          {
            $push: {
              history: {
                status: "ON_THE_WAY",
                motoboy: motoboy.name,
                timestamp: new Date(),
              },
            },
          }
        );
      }
    );
  });
}
