import axios from "axios";
import { strict as assert } from "assert";

const gateway: string = "http://localhost:3000";
const customerService: string = gateway;
const deliveryService: string = gateway;
const orderQueryService: string = gateway;
const orderService: string = gateway;

function sleep(ms: number) {
  return new Promise((res) => setTimeout(res, ms));
}

let customer: any = {
  name: "joão",
  opening_balance: 100,
};

let motoboy: any = {
  name: "pedro",
};

let orderOK: any = {
  items: [
    { name: "milkshake", amount: 1, price: 2.5 },
    { name: "sanduiche", amount: 2, price: 4.5 },
  ],
};

const orderFailCustomer: any = {
  customer_id: "aduahfasd",
  items: [
    { name: "milkshake", amount: 1, price: 2.5 },
    { name: "sanduiche", amount: 2, price: 4.5 },
  ],
};

const orderFailLimit: any = {
  items: [
    { name: "milkshake", amount: 100, price: 2.5 },
    { name: "sanduiche", amount: 200, price: 4.5 },
  ],
};

async function runTests(): Promise<void> {
  const customerCreationRes: any = await axios.post(
    `${customerService}/customers`,
    customer
  );
  console.error("[CREATE CUSTOMER]: ", customerCreationRes.data);
  await sleep(500);

  const customerQueryRes: any = await axios.get(
    `${customerService}/customers/${customerCreationRes.data.customer.uuid}`
  );
  console.error("[QUERY CUSTOMER] ", customerQueryRes.data);
  await sleep(500);

  assert.equal(
    customerQueryRes.data.customer.name,
    customer.name,
    `received ${customerQueryRes.data.customer.name}, expected ${customer.name}`
  );

  customer = { ...customerCreationRes.data.customer };

  const motoboyCreationRes: any = await axios.post(
    `${deliveryService}/motoboys`,
    motoboy
  );
  console.error("[CREATE MOTOBOY] ", motoboyCreationRes.data);
  await sleep(500);

  const motoboyQueryRes: any = await axios.get(
    `${deliveryService}/motoboys/${motoboyCreationRes.data.motoboy.uuid}`
  );
  console.error("[QUERY MOTOBOY] ", motoboyQueryRes.data);
  await sleep(500);

  assert.equal(
    motoboyQueryRes.data.motoboy.name,
    motoboy.name,
    `received ${motoboyQueryRes.data.motoboy.name}, expected ${motoboy.name}`
  );

  motoboy = { ...motoboyQueryRes.data.motoboy };

  const motoboyUpdateRes: any = await axios.put(
    `${deliveryService}/motoboys/${motoboyCreationRes.data.motoboy.uuid}`
  );
  console.error("[UPDATE MOTOBOY] ", motoboyUpdateRes.data);
  await sleep(500);

  orderOK.customer_id = customer.uuid;

  const orderCreationRes: any = await axios.post(
    `${orderService}/orders`,
    orderOK
  );
  console.error("[CREATE ORDER] ", orderCreationRes.data);
  await sleep(500);

  await sleep(500);

  const orderQueryRes: any = await axios.get(
    `${orderQueryService}/orders/${orderCreationRes.data.order.uuid}`
  );
  console.error("[QUERY ORDER] ", orderQueryRes.data);

  assert.equal(
    orderQueryRes.data.order.customer_id,
    orderOK.customer_id,
    `received ${orderQueryRes.data.order.customer_id}, expected ${orderOK.customer_id}`
  );

  const orderHistory: any[] = orderQueryRes.data.order.history;
  assert.equal(
    orderHistory[orderHistory.length - 1].status,
    "ON_THE_WAY",
    `received ${orderQueryRes.data.order.status}, expected ON_THE_WAY`
  );
}

setTimeout(runTests, 1000);
