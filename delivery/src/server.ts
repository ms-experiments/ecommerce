import * as Koa from "koa";
import * as bodyParser from "koa-bodyparser";
import * as json from "koa-json";
import * as logger from "koa-logger";
import * as Router from "koa-router";
import { v4 as uuid } from "uuid";

export const app: Koa = new Koa();

app.use(json());
app.use(logger());
app.use(bodyParser());

const router: Router = new Router();

async function validateMotoboy({ name }: any, db: any): Promise<any> {
  if (!name || name.length < 3) {
    return {
      isValid: false,
      reason: "invalid name",
    };
  }

  const motoboy: any = await db.collection("motoboys").findOne({ name });
  if (motoboy) {
    return {
      isValid: false,
      reason: "motoboy already exists",
    };
  }

  return { isValid: true };
}

router.get(
  "/health",
  async (ctx: any): Promise<void> => {
    const dbStats = await ctx.db.stats();

    ctx.body = { db: dbStats };
  }
);

router.post(
  "/motoboys",
  async (ctx: any): Promise<void> => {
    const { isValid, reason }: any = await validateMotoboy(
      ctx.request.body,
      ctx.db
    );

    if (!isValid) {
      ctx.body = { error: reason };
    } else {
      const motoboy = {
        ...ctx.request.body,
        uuid: uuid(),
        status: "OFF",
      };

      const event = {
        type: "MOTOBOY_CREATED",
        message: motoboy,
      };

      ctx.broker.publish(event.type, JSON.stringify(event));
      ctx.body = { motoboy };
    }
  }
);

router.put(
  "/motoboys/:id",
  async (ctx: any): Promise<void> => {
    const { db, broker }: any = ctx;
    const { id }: any = ctx.params;

    const motoboy: any = await db.collection("motoboys").findOne({ uuid: id });

    if (!motoboy) {
      ctx.body = { error: "motoboy does not exist - " + id };
    } else {
      ctx.body = { msg: "ok" };

      const event: any = {
        type: "MOTOBOY_FREED",
        message: motoboy,
      };

      broker.publish(event.type, JSON.stringify(event));
    }
  }
);

router.get(
  "/motoboys/:uuid",
  async (ctx: any): Promise<void> => {
    const {
      db,
      params: { uuid },
    }: any = ctx;

    const motoboy: any = await db.collection("motoboys").findOne({ uuid });

    ctx.body = { motoboy };
  }
);

app.use(router.routes());
app.use(router.allowedMethods());
