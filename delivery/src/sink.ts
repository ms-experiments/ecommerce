import {
  connect,
  Stan,
  Message,
  SubscriptionOptions,
} from "node-nats-streaming";
import { v4 as uuid } from "uuid";

export const broker: Stan = connect("test-cluster", "delivery-svc", {
  url: "nats://broker:4222",
});

export function setupBroker(db: any) {
  broker.on("connect", () => {
    const replayAll: SubscriptionOptions = broker
      .subscriptionOptions()
      .setDeliverAllAvailable();

    broker.subscribe("MOTOBOY_CREATED", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const { message }: any = JSON.parse(msg.getData() as string);

        await db.collection("motoboys").insertOne(message);
      }
    );

    broker.subscribe("MOTOBOY_FREED", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const { message }: any = JSON.parse(msg.getData() as string);
        const { uuid }: any = message;

        const pack: any = await db
          .collection("packages")
          .findOne({ status: "READY" });

        if (!pack) {
          await db
            .collection("motoboys")
            .findOneAndUpdate({ uuid }, { $set: { status: "FREE" } });
        } else {
          const event: any = {
            type: "PACKAGE_OUT_FOR_DELIVERY",
            message: {
              package: pack,
              motoboy: message,
            },
          };

          broker.publish(event.type, JSON.stringify(event));
        }
      }
    );

    broker.subscribe("PACKAGE_OUT_FOR_DELIVERY", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const { message }: any = JSON.parse(msg.getData() as string);
        await db
          .collection("packages")
          .findOneAndUpdate(
            { uuid: message.uuid },
            { $set: { status: "OUT_FOR_DELIVERY" } }
          );
      }
    );

    broker.subscribe("ORDER_PLACED", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const { message }: any = JSON.parse(msg.getData() as string);

        const pack: any = {
          uuid: uuid(),
          order: { ...message },
          status: "READY",
        };

        const event: any = {
          type: "PACKAGE_READY",
          message: pack,
        };

        broker.publish(event.type, JSON.stringify(event));
      }
    );

    broker.subscribe("PACKAGE_READY", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const { message }: any = JSON.parse(msg.getData() as string);

        const motoboy: any = await db
          .collection("motoboys")
          .findOne({ status: "FREE" });

        if (!motoboy) {
          await db.collection("packages").insertOne(message);
        } else {
          const event: any = {
            type: "PACKAGE_OUT_FOR_DELIVERY",
            message: {
              package: message,
              motoboy,
            },
          };

          broker.publish(event.type, JSON.stringify(event));
        }
      }
    );
  });
}
