# E-Commerce

This repo is a playground for microservices.


## Setup the platform

To setup the platform, run

> please, verify if you have added your user to the docker group in order to run docker commands without sudo

```sh
# build all local images
docker-compose build

# run all containers
docker-compose up -d
```

## Testing

To run the tests, run

```
# change to the tests dir
cd tests/

# install dependencies
yarn install

# run the test command
yarn dev
```

## To-Do:
- [ ] dockerize the tests

## Architecture

```
                          +-------------+
//========================| API Gateway |========================\\
|| (Virtual Network)      +-------------+                        ||
||                                                               ||
||                                                               ||
|| +--------+   +----------------+   +----------+   +----------+ ||
|| | Orders |   | Orders History |   | Delivery |   | Customer | ||
|| +--------+   +----------------+   +----------+   +----------+ ||
||                                                               ||
||                                                               ||
||                       +----------------+                      ||
||                       | Message Broker |                      ||
||                       +----------------+                      ||
||                                                               ||
\\===============================================================//
```
