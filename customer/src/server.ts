import * as Koa from "koa";
import * as bodyParser from "koa-bodyparser";
import * as json from "koa-json";
import * as logger from "koa-logger";
import * as Router from "koa-router";
import { v4 as uuid } from "uuid";

export const app: Koa = new Koa();

app.use(json());
app.use(logger());
app.use(bodyParser());

const router: Router = new Router();

async function validateCustomer(params: any, db: any): Promise<any> {
  const { name, opening_balance }: any = params;

  if (!opening_balance || opening_balance < 0) {
    return {
      isValid: false,
      reason: "invalid opening_balance",
    };
  }

  if (!name || name.length == 3) {
    return {
      isValid: false,
      reason: "invalid name",
    };
  }

  const customer = await db.collection("customers").findOne({ name });

  if (customer) {
    return {
      isValid: false,
      reason: "customer already exists",
    };
  }

  return { isValid: true };
}

router.get(
  "/health",
  async (ctx: any): Promise<void> => {
    const dbStats = await ctx.db.stats();

    ctx.body = { db: dbStats };
  }
);

router.post(
  "/customers",
  async (ctx: any): Promise<void> => {
    const { isValid, reason }: any = await validateCustomer(
      ctx.request.body,
      ctx.db
    );

    if (!isValid) {
      ctx.body = { error: reason };
    } else {
      const customer = {
        name: ctx.request.body.name,
        balance: ctx.request.body.opening_balance,
        uuid: uuid(),
      };

      const event = {
        type: "CUSTOMER_CREATED",
        message: customer,
      };

      ctx.broker.publish(event.type, JSON.stringify(event));
      ctx.body = { customer };
    }
  }
);

router.get(
  "/customers/:uuid",
  async (ctx: any): Promise<void> => {
    const {
      db,
      params: { uuid },
    }: any = ctx;
    const customer: any = await db.collection("customers").findOne({ uuid });

    ctx.body = { customer };
  }
);

app.use(router.routes());
app.use(router.allowedMethods());
