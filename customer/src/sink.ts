import {
  connect,
  Stan,
  Message,
  SubscriptionOptions,
} from "node-nats-streaming";

export const broker: Stan = connect("test-cluster", "customer-svc", {
  url: "nats://broker:4222",
});

export function setupBroker(db: any) {
  broker.on("connect", () => {
    const replayAll: SubscriptionOptions = broker
      .subscriptionOptions()
      .setDeliverAllAvailable();

    broker.subscribe("CUSTOMER_CREATED", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const { message }: any = JSON.parse(msg.getData() as string);
        await db.collection("customers").insertOne(message);
      }
    );

    broker.subscribe("ORDER_CREATED", replayAll).on(
      "message",
      async (msg: Message): Promise<void> => {
        const { message }: any = JSON.parse(msg.getData() as string);
        const { customer_id, items }: any = message;

        const customer: any = await db
          .collection("customers")
          .findOne({ uuid: customer_id });

        const event: any = { type: null, message };
        if (!customer) {
          event.type = "CUSTOMER_NOT_FOUND";
        } else {
          const total: number = items.reduce(
            (subtotal: number, { price, amount }: any): number =>
              subtotal + price * amount,
            0
          );

          if (total > customer.balance) {
            event.type = "CREDIT_LIMIT_EXCEEDED";
          } else {
            event.type = "CREDIT_RESERVED";
            const balance: number = customer.balance - total;
            await db
              .collection("customers")
              .findOneAndUpdate({ uuid: customer_id }, { $set: { balance } });
          }
        }

        broker.publish(event.type, JSON.stringify(event));
      }
    );
  });
}
