import * as Koa from "koa";
import * as Router from "koa-router";
import * as bodyParser from "koa-bodyparser";
import * as logger from "koa-logger";
import * as cors from "@koa/cors";
import axios from "axios";

const { PORT }: NodeJS.ProcessEnv = process.env;

const port: number = Number.parseInt(PORT || "3000");

const app: Koa = new Koa();

app.use(bodyParser());
app.use(logger());
app.use(cors());

const router: Router = new Router();

const customersBaseURL: string = "http://customers:9090";

router.post(
  "/customers",
  async (ctx: Koa.Context): Promise<void> => {
    const { data }: any = await axios.post(`${customersBaseURL}/customers`, {
      ...ctx.request.body,
    });

    ctx.body = { ...data };
  }
);

router.get(
  "/customers/:id",
  async (ctx: Koa.Context): Promise<void> => {
    const { data }: any = await axios.get(
      `${customersBaseURL}/customers/${ctx.params.id}`
    );

    ctx.body = { ...data };
  }
);

const deliveriesBaseURL: string = "http://deliveries:9090";

router.post(
  "/motoboys",
  async (ctx: Koa.Context): Promise<void> => {
    const { data }: any = await axios.post(`${deliveriesBaseURL}/motoboys`, {
      ...ctx.request.body,
    });

    ctx.body = { ...data };
  }
);

router.get(
  "/motoboys/:id",
  async (ctx: Koa.Context): Promise<void> => {
    const { data }: any = await axios.get(
      `${deliveriesBaseURL}/motoboys/${ctx.params.id}`
    );

    ctx.body = { ...data };
  }
);

router.put(
  "/motoboys/:id",
  async (ctx: Koa.Context): Promise<void> => {
    const { data }: any = await axios.put(
      `${deliveriesBaseURL}/motoboys/${ctx.params.id}`
    );

    ctx.body = { ...data };
  }
);

const ordersHistoryBaseURL: string = "http://orders_history:9090";

router.get(
  "/orders",
  async (ctx: Koa.Context): Promise<void> => {
    const { data }: any = await axios.get(`${ordersHistoryBaseURL}/orders`);

    ctx.body = { ...data };
  }
);

router.get(
  "/orders/:id",
  async (ctx: Koa.Context): Promise<void> => {
    const { data }: any = await axios.get(
      `${ordersHistoryBaseURL}/orders/${ctx.params.id}`
    );

    ctx.body = { ...data };
  }
);

const ordersBaseURL: string = "http://orders:9090";

router.post(
  "/orders",
  async (ctx: Koa.Context): Promise<void> => {
    const { data }: any = await axios.post(`${ordersBaseURL}/orders`, {
      ...ctx.request.body,
    });

    ctx.body = { ...data };
  }
);

router.get(
  "/health",
  async (ctx: Koa.Context): Promise<void> => {
    const allStatus: any = {};
    const all: any[] = [
      { name: "customers", url: customersBaseURL },
      { name: "deliveries", url: deliveriesBaseURL },
      { name: "orders", url: ordersBaseURL },
      { name: "orders_history", url: ordersHistoryBaseURL },
    ];

    for (const one of all) {
      const { data }: any = await axios.get(`${one.url}/health`);
      allStatus[one.name] = data;
    }

    ctx.body = { allStatus };
  }
);

app.use(router.routes()).use(router.allowedMethods());

app.listen(port, () => console.log("gateway running"));
